package org.wsfta.model.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathExpressionException;

import org.wsfta.model.event.NotifierEvent;
import org.wsfta.model.event.NotifierEventListener;
import org.wsfta.model.group.Membership;
import org.wsfta.model.group.Message;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 12/2006
 */
public abstract class ServiceManager implements NotifierEventListener
{
	private Membership leaderMembership;

	private Membership groupMembership;

	private Collection<NotifierEventListener> notifierListeners;

	/**
	 * 
	 * @param listener
	 * @return
	 */
	public Boolean addNotifierEventListener(NotifierEventListener listener)
	{
		return this.notifierListeners.add(listener);
	}

	/**
	 * 
	 * @param event
	 */
	public void fireNotifierEvent(NotifierEvent event)
	{
		for (NotifierEventListener listener : this.notifierListeners)
			listener.notify(event);
	}

	/**
	 * 
	 * @return
	 */
	public Membership getLeaderMembership()
	{
		return this.leaderMembership;
	}
	/**
	 * 
	 * @return
	 */
	public Membership getGroupMembership()
	{
		return this.groupMembership;
	}
	
	/**
	 * 
	 * @param listener
	 * @return
	 */
	public Boolean removeNotifierListener(NotifierEventListener listener)
	{
		return this.notifierListeners.remove(listener);
	}
	
	/**
	 * 
	 * @param membership
	 */
	public void setLeaderMembership(Membership membership)
	{
		this.leaderMembership = membership;
	}
	
	/**
	 * 
	 * @param membership
	 */
	public void setGroupMembership(Membership membership)
	{
		this.groupMembership = membership;
	}
	
	
	/**
	 * 
	 * @param notifierListeners
	 */
	public void setNotifierListeners(Collection<NotifierEventListener> notifierListeners)
	{
		this.notifierListeners = notifierListeners;
	}

	/**
	 * 
	 * @param messages
	 * @param messageTrash
	 * @return
	 * @throws SOAPException
	 * @throws XPathExpressionException
	 */
	public abstract Message voting(ArrayList<Message> messages, ArrayList<Object> messageTrash) throws SOAPException, XPathExpressionException;
	
	/**
	 * 
	 * @param message
	 */
	public abstract void waitForOrderExecution(SOAPMessage soapMessage) throws InterruptedException;
}