package org.wsfta.model.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.wsfta.model.event.NotifierEvent;
import org.wsfta.model.group.Membership;
import org.wsfta.model.group.Message;
import org.wsfta.model.group.soap.SOAPMember;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 12/2006
 */
public class ServiceManagerImpl extends ServiceManager
{
	private final Lock execLock = new ReentrantLock();
	
	private final Lock replyLock = new ReentrantLock();
	
	private final Condition isReadExec = this.execLock.newCondition();
	
    private Deque<Message> messagesForExecution;
	
	private Map<Long, List<Message>> replyMap;
	
	private XPath xPath;
	
	private final ExecutorService scheduler = Executors.newFixedThreadPool(2);
	
	private ChannelHandler leaderHandler;
	
	private ChannelHandler groupHandler;
	
	private class ChannelHandler implements Runnable
	{
	    private final Membership membership;
	    
	    private final AtomicBoolean active = new AtomicBoolean(true);
	    
	    public ChannelHandler(final Membership membership)
        {
            // TODO Auto-generated constructor stub
	        this.membership = membership;
        }

        /**
         * @param active the active to set
         */
        public void setActive(final Boolean state)
        {
            this.active.set(state);
        }

        @Override
	    public void run()
	    {
            Message message = null;
            List<Message> auxList = null;
	        // TODO Auto-generated method stub
            while(this.active.get())
            {
                message = this.membership.receiveMessage();
                
                if(message.getContent() instanceof Object[])
                {
                    ServiceManagerImpl.this.replyLock.lock();
                    auxList = ServiceManagerImpl.this.replyMap.get(message.getOid());
                    auxList.add(message);
                    ServiceManagerImpl.this.replyLock.unlock();
                }
                else
                {
                    ServiceManagerImpl.this.execLock.lock();
                    ServiceManagerImpl.this.messagesForExecution.addLast(message);
                    ServiceManagerImpl.this.execLock.unlock();
                }
            }
	    }
	}
	
	public ServiceManagerImpl()
    {
        // TODO Auto-generated constructor stub
	    this.leaderHandler = new ChannelHandler(this.getLeaderMembership());
	    this.groupHandler = new ChannelHandler(this.getGroupMembership());
	    this.scheduler.execute(this.leaderHandler);
	    this.scheduler.execute(this.groupHandler);
    }
	
	public Message voting(ArrayList<Message> messages, ArrayList<Object> messageTrash) throws SOAPException, XPathExpressionException
	{
		Message receiveMessage = null;
		SOAPMessage soapMessage = null;
		String maxResultLocal = null;
		String result = null;
		Message maxOccurrence = null;
		Message maxOccurrenceLocal = null;
		Integer nrTimes = 0;
		Integer nrTimesLocal = 0;
		String expression = "//return/text()";
		
		for (int i = 0; i < messages.size(); i++)
		{
			maxOccurrenceLocal = messages.get(i);
			nrTimesLocal = 1;
			soapMessage = (SOAPMessage)maxOccurrenceLocal.getContent();
			
			maxResultLocal = (String)this.xPath.evaluate(expression, soapMessage.getSOAPBody());
			
			if(maxOccurrence == null || (maxOccurrence != null && !messageTrash.contains(maxResultLocal)))
			{
				for (int j = i+1; j < messages.size(); j++)
				{
					receiveMessage = messages.get(j);
					soapMessage = (SOAPMessage)receiveMessage.getContent();
					result = (String) this.xPath.evaluate(expression, soapMessage.getSOAPBody());
					
					if(maxResultLocal.equals(result))
						nrTimesLocal++;
				}
				
				messageTrash.add(maxResultLocal);
				if(nrTimes < nrTimesLocal)
				{
					nrTimes = nrTimesLocal;
					maxOccurrence = maxOccurrenceLocal;
				}
			}
		}
		
		return maxOccurrence;
	}
	
	/**
     * @param member
     */
	public ServiceManagerImpl(SOAPMember member, Collection<Message> messageWillExecutedCollection)
	{	
	    
		XPathFactory xPathFactory = XPathFactory.newInstance();
		this.xPath = xPathFactory.newXPath();
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.service.Service#waitForOrderExecution(org.wsfta.model.group.Message)
	 */
	@Override
	public void waitForOrderExecution(SOAPMessage soapMessage) throws InterruptedException
	{
		// TODO Auto-generated method stub
	    SOAPMessage auxMsg;
	    this.execLock.lock();
    	    while(true)
            {   	        
    	        if(!this.messagesForExecution.isEmpty())
    	        {
        	        auxMsg = (SOAPMessage)this.messagesForExecution.peek().getContent();
        	        if(auxMsg.equals(soapMessage))
        	        {
        	            this.messagesForExecution.poll();
        	            this.isReadExec.signalAll();
        	            break;
        	        }
    	        }
    	        
    	        this.isReadExec.await();
            }
    	 this.execLock.unlock();   
	}

    @Override
    public void notify(NotifierEvent event)
    {
        // TODO Auto-generated method stub
        
    }
}