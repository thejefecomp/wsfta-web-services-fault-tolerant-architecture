/**
 * 
 */
package org.wsfta.model.ordering;

/**
 * @author Jeferson luiz Rodrigues Souza
 *
 */
public class OrderRecord
{
	
	private String memberOid;
	
	private Long messageOid;

	public OrderRecord(String memberOid, Long messageOid)
	{
		this.memberOid = memberOid;
		this.messageOid = messageOid;
	}
	
	/**
	 * @return the memberOid
	 */
	public String getMemberOid()
	{
		return this.memberOid;
	}

	/**
	 * @return the messageOid
	 */
	public Long getMessageOid()
	{
		return this.messageOid;
	}
}