package org.wsfta.model.ordering;


import org.wsfta.model.ordering.impl.TotalMessageOrder;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 09/2006
 */
public class MessageOrderFactory
{

    public MessageOrder getNewInstance(String algorithm)
    {
    	MessageOrder messageOrderImpl = null;
    	if(algorithm.equalsIgnoreCase("TotalOrder"))
    	{
    		messageOrderImpl = new TotalMessageOrder();
    	}
    	
    	return messageOrderImpl;
    }
}