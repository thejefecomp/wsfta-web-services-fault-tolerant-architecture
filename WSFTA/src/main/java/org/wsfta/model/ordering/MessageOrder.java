package org.wsfta.model.ordering;

import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.concurrent.atomic.AtomicBoolean;

import org.wsfta.model.event.NotifierEventListener;
import org.wsfta.model.event.NotifierEventSource;
import org.wsfta.model.group.Message;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 11/2006
 */
public abstract class MessageOrder extends NotifierEventSource implements NotifierEventListener, Runnable
{
	
	private AtomicBoolean orderer;
	
	 protected List<Message> buffer;
	    
     protected List<Message> msgs2Order;
    
     protected Map<String, Long> lastInsertedId;
    
     protected Map<String, SortedSet<Message>> msgOutOfOrder; 
    
     protected Integer  nrMessageWindow;

     protected Long timeOfOrder;
     
    /**
     * @return the buffer
     */
    public List<Message> getBuffer()
    {
        return this.buffer;
    }

    /**
     * @param buffer the buffer to set
     */
    public void setBuffer(List<Message> buffer)
    {
        this.buffer = buffer;
    }
    
    /**
     * @return the msgs2Order
     */
    public List<Message> getMsgs2Order()
    {
        return this.msgs2Order;
    }

    /**
     * @param msgs2Order the msgs2Order to set
     */
    public void setMsgs2Order(List<Message> msgs2Order)
    {
        this.msgs2Order = msgs2Order;
    }

    /**
     * @return the lastInsertedId
     */
    public Map<String, Long> getLastInsertedId()
    {
        return this.lastInsertedId;
    }

    /**
     * @param lastInsertedId the lastInsertedId to set
     */
    public void setLastInsertedId(Map<String, Long> lastInsertedId)
    {
        this.lastInsertedId = lastInsertedId;
    }

    /**
     * @return the msgOutOfOrder
     */
    public Map<String, SortedSet<Message>> getMsgOutOfOrder()
    {
        return this.msgOutOfOrder;
    }

    /**
     * @param msgOutOfOrder the msgOutOfOrder to set
     */
    public void setMsgOutOfOrder(Map<String, SortedSet<Message>> msgOutOfOrder)
    {
        this.msgOutOfOrder = msgOutOfOrder;
    }

    /**
     * @return the nrMessageWindow
     */
    public Integer getNrMessageWindow()
    {
        return this.nrMessageWindow;
    }

    /**
     * @param nrMessageWindow the nrMessageWindow to set
     */
    public void setNrMessageWindow(Integer nrMessageWindow)
    {
        this.nrMessageWindow = nrMessageWindow;
    }

    /**
     * @return the timeOfOrder
     */
    public Long getTimeOfOrder()
    {
        return this.timeOfOrder;
    }

    /**
     * @param timeOfOrder the timeOfOrder to set
     */
    public void setTimeOfOrder(Long timeOfOrder)
    {
        this.timeOfOrder = timeOfOrder;
    }

    /**
     * Método Responsável por adicionar mensagens para ordenação. Recebe um objeto que representa a mensagem que será ordenada e o Id do buffer que essa mensagem deve ser colocada.
     * @param message Objeto que representa a mensagem que deve ser ordenada 
     * @return Retorna <code>true</code> se a mensagem pode ser inserida. Retorna <code>false</code> caso contrário.
     */
    public abstract void addMessage(Message message);
    
    /**
     * Método que define a ordem de execução das mensagens. Em caso de implementações de ordenação otimista, esse método não realiza nenhuma tarefa já que a ordem já é pré-definida. 
     *
     */
    public abstract void defineOrder();

	/**
     * Método que executa a ordenação das mensagens. Esse método é responsável por indicar a ordem da mensagem, repassando-as para seus observadores interessados nessa ordenação. 
     *
     */
    public abstract void order(Integer nrMsgs2Order);

	/**
	 * @return the orderer
	 */
	public Boolean isOrderer()
	{
		return this.orderer.get();
	}

	/**
	 * @param orderer the orderer to set
	 */
	public void setOrderer(AtomicBoolean orderer)
	{
		this.orderer = orderer;
	}
	
	public void setOrderer(Boolean order)
	{
		this.orderer.set(order);
	}
}