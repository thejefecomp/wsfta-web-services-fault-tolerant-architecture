package org.wsfta.model.ordering.impl;

import org.wsfta.model.ordering.MessageMarker;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 01/2007
 */
public class MessageMarkerImpl implements MessageMarker
{
	private Long nextId;

	public MessageMarkerImpl()
    {
        // TODO Auto-generated constructor stub
	    this.nextId = 0L;
	 
    }
	
	/**
     * @see org.wsfta.model.ordering.MessageMarker#setSeed(java.lang.Long)
     */
	public void setSeed(Long seed)
	{
		this.nextId = seed;
	}

	/**
     * @see org.wsfta.model.ordering.MessageMarker#getNextId()
     */
	public synchronized Long getNextId()
	{
		this.nextId = this.nextId+1;
		
		return this.nextId;
	}
}