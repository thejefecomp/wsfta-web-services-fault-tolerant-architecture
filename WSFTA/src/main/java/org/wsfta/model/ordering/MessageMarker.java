package org.wsfta.model.ordering;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 01/2007
 */
public interface MessageMarker
{
	/**
	 * Método responsável por atribuir um valor inicial (semente) que será utilizada como base para a geração automática de Id's.
	 * @param seed valor inicial atribuido para a geração de Id's.
	 */
	public void setSeed(Long seed);

	/**
	 * Método responsável por retornar o próximo Id válido gerado.
	 * @return próximo Id válido.
	 */
	public Long getNextId();
}