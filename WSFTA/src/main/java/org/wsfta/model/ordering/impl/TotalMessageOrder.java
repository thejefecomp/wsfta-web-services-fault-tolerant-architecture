/**
 * 
 */
package org.wsfta.model.ordering.impl;

import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.wsfta.model.event.NotifierEvent;
import org.wsfta.model.group.Message;
import org.wsfta.model.ordering.MessageOrder;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * 
 */
public class TotalMessageOrder extends MessageOrder
{   
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    
    private ScheduledFuture<TotalMessageOrder> timeoutSchedule;
    
    private final Lock lock = new ReentrantLock(); 
    
    private void makeOrderList(List<Message> orderList)
    {
        this.lock.lock();
        NotifierEvent event = new NotifierEvent(this);
        this.msgs2Order.clear();
        for (Message message : orderList)
            this.msgs2Order.add(this.buffer.remove(this.buffer.indexOf(message)));
        
        event.setData(this.msgs2Order);
        this.notifyListeners(event);
        this.lock.unlock();
    }
    
    private void verifyMsgOutOrder(String source)
    {
        
        SortedSet<Message> auxSet = this.msgOutOfOrder.get(source);
        
        if(!auxSet.isEmpty())
        {
            Message auxMsg = auxSet.first();
            while(this.lastInsertedId.get(source) + 1 == auxMsg.getOid())
            {
                this.lastInsertedId.put(source, auxMsg.getOid());
                this.buffer.add(auxMsg);
                auxSet.remove(auxMsg);
                auxMsg = auxSet.first();
            }
        }
    }
    
    /* (non-Javadoc)
     * @see org.wsfta.model.ordering.MessageOrder#addMessage(org.wsfta.model.group.Message)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void addMessage(Message message)
    {
        // TODO Auto-generated method stub
        this.lock.lock();
        if(this.buffer.contains(message)) 
        {
            this.lock.unlock();
            return;
        }
        
        if(this.lastInsertedId.get(message.getSource()) + 1 == message.getOid())
        {
            this.lastInsertedId.put(message.getSource(), message.getOid());
            this.buffer.add(message);
            synchronized(System.out)
            {
                System.out.println("Adicionou mensagem: "+message.getOid()+" do membro: "+message.getSource());
            }
        }
        else
            this.msgOutOfOrder.get(message.getSource()).add(message);
        
        this.verifyMsgOutOrder(message.getSource());
        
        if(this.isOrderer())
            this.defineOrder();
        
        if(this.isOrderer() && (this.timeoutSchedule == null || this.timeoutSchedule.isDone() || this.timeoutSchedule.isCancelled()))
        {
            synchronized(System.out)
            {
                System.out.println("Disparando timeout de Ordenação !!!");
            }
            this.timeoutSchedule = (ScheduledFuture<TotalMessageOrder>) this.scheduler.schedule(this, this.timeOfOrder, TimeUnit.MILLISECONDS);
        }
        this.lock.unlock();
    }

    /* (non-Javadoc)
     * @see org.wsfta.model.ordering.MessageOrder#defineOrder()
     */
    @Override
    public void defineOrder()
    {
        // TODO Auto-generated method stub
        if(this.buffer.size() >= this.nrMessageWindow)
        {
            this.timeoutSchedule.cancel(true);
            synchronized(System.out)
            {
                System.out.println("CANCEL TIMEOUT");
            }
            this.order(this.buffer.size());
        }
    }

    /* (non-Javadoc)
     * @see org.wsfta.model.ordering.MessageOrder#order()
     */
    @Override
    public void order(Integer nrMsgs2Order)
    {
        // TODO Auto-generated method stub
        NotifierEvent event = new NotifierEvent(null);
        
        this.msgs2Order.clear();
        for (int i = nrMsgs2Order; i > 0; i--)
            this.msgs2Order.add(this.buffer.remove(0));
        
        event.setData(this.msgs2Order);
        this.notifyListeners(event);
    }

    /* (non-Javadoc)
     * @see org.wsfta.model.event.NotifierEventListener#notify(org.wsfta.model.event.NotifierEvent)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void notify(NotifierEvent event)
    {
        //Para ataques maliciosos seria necessário validar a origem da mensagem !!!
        Object data = event.getData();
        
        if(data instanceof Message)
        {
            Message msg = (Message)data;

            if(this.isOrderer())
                this.notifyListeners(event);
            
            this.addMessage(msg);
        }
        else if(data instanceof List)
            this.makeOrderList((List<Message>)data);
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run()
    {
        // TODO Auto-generated method stub
        synchronized(System.out)
        {
            System.out.println("THREAD de ORDENAÇÃO sendo executada !!!");
        }
        this.lock.lock();
        if(this.buffer.size() > 0)
            this.order(TotalMessageOrder.this.buffer.size());       
        this.lock.unlock();
    }
}