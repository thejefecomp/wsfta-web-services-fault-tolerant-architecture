package org.wsfta.model.fd.soap.wrapper;

import java.io.Serializable;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.wsfta.model.fd.FailureDetector;
import org.wsfta.model.fd.MonitorableComponent;
import org.wsfta.model.group.Member;

/**
 * @author Jeferson
 *
 */
public class WebServiceWrapper implements MonitorableComponent, Serializable
{
	private static final long	serialVersionUID	= 2777967824852586916L;
	
	private URL serviceEndpoint;
	
	private ExecutorService scheduler;
	
	private FailureDetector detector;
	
	private SOAPConnection soapConnection;
	
	private MessageFactory messageFactory;
	
	public WebServiceWrapper(URL serviceEndpoint, ExecutorService scheduler, SOAPConnection soapConnection, MessageFactory messageFactory) throws SOAPException
	{
		this.serviceEndpoint = serviceEndpoint;
		this.scheduler = scheduler;
		this.soapConnection = soapConnection;
		this.messageFactory = MessageFactory.newInstance();
	}
	
	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.MonitorableComponent#getStatus()
	 */
	@Override
	public void getStatus() throws RemoteException
	{
		// TODO Auto-generated method stub
		this.scheduler.execute(this);
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.MonitorableComponent#updateFD()
	 */
	
	@Override
	public void updateFD()
	{
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		try
		{ 
			SOAPMessage message = messageFactory.createMessage();
			SOAPBody messageBody = message.getSOAPPart().getEnvelope().getBody();
			
			messageBody.addBodyElement(messageBody.createQName("getFdStatus", messageBody.getPrefix()));
			message.saveChanges();
			soapConnection.call(message, this.serviceEndpoint);
			this.detector.updateServiceInterval();
		}
		catch (SOAPException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (RemoteException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.MonitorableComponent#setDetector(org.wsfta.model.fd.FailureDetector)
	 */
	@Override
	public void setDetector(FailureDetector detector)
	{
		// TODO Auto-generated method stub
		this.detector = detector;
	}
	
	@Override
	public void setHeartbeatInterval(Long heartbeatInterval)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void updateMembership(Member member, String fdType, Boolean operationType)
	{
		// TODO Auto-generated method stub
		
	}
}