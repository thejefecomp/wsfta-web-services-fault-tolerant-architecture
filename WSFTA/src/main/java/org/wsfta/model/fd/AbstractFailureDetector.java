package org.wsfta.model.fd;

import java.util.Collection;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.wsfta.model.group.Member;
import org.wsfta.model.group.Membership;
import org.wsfta.model.event.NotifierEventListener;
import org.wsfta.model.event.NotifierEvent;

public abstract class AbstractFailureDetector implements FailureDetector, Member
{
	/**
	 * atributo de identificação única do detector de falha;
	 */
	protected String oid;
	
	/**
	 *Componente monitorado (Pode ser um Wrapper para um serviço remoto ou Local sendo executado em outro processo);
	 */
	protected MonitorableComponent component;

	/**
	 * Grupo de detectores de falha;
	 */
	protected Membership failureDetectors;

	/**
	 * lista de objetos que serão notificados;
	 */
	protected Collection<NotifierEventListener> notifierListeners;

	/**
	 * intervalo de monitoração do próximo detector de falha no anel lógico;
	 */
	private Long fdMonitorInterval;
	
	/**
	 * Timeout que serve como base para suspeita de falha do próximo detector de falha no anel lógico;
	 */
	protected Long fdTimeout;
	
	/**
	 * intervalo de monitoração;
	 */
	private Long monitorInterval;
	
	/**
	 * Timeout que serve como base para suspeita de falha do componente monitorado;
	 */
	protected Long timeout;
	
	/**
	 * Número de tentativas necessárias para decretar a falha do componente;
	 */
	protected Integer nrOfAttempts;
	
	/**
	 * Número de tentativas necessárias para decretar a falha do detector vizinho no anel lógico;
	 */
	protected Integer nrOfFDAttempts;
	
	/**
	 * Número de tentativas efetuadas na tentativa de identificar a falha do componente;
	 */
	protected AtomicInteger nrTimes;
	
	/**
	 * N�mero de tentativas efetuadas na tentativa de identificar a falha do detector vizinho no anel l�gico;
	 */
	protected AtomicInteger nrFdTimes;
	
	/**
	 * Indica que o detector de falha está ativo. (true = ativo, false = inativo);
	 */
	protected AtomicBoolean isMonitoring;
	
	/**
	 * Quantidade de milisegundos do relógio local que indica o momento da recepção de uma resposta ou de um hearthbeat do componente monitorado;
	 */
	protected Long lastReceiveTime;
	
	/**
	 * Objeto para escalonamento de tarefas;
	 */
	protected ScheduledExecutorService scheduler;
	
	/**
	 * Objeto que representa a tarefa que foi escalonada;
	 */
	protected ScheduledFuture checkTimeout;
	
	public void getInstance(String className)
	{
	}
	
	/**
	 * @param oid the oid to set
	 */
	public void setOid(String oid)
	{
		this.oid = oid;
	}

	/**
	 * @param scheduler the scheduler to set
	 */
	public void setScheduler(ScheduledExecutorService scheduler)
	{
		this.scheduler = scheduler;
	}
	
	

	/**
	 * @param nrTimes the nrTimes to set
	 */
	public void setNrTimes(AtomicInteger nrTimes)
	{
		this.nrTimes = nrTimes;
	} 
	
	/**
	 * @return the fdMonitorInterval
	 */
	public Long getFdMonitorInterval()
	{
		return this.fdMonitorInterval;
	}

	/**
	 * @param fdMonitorInterval the fdMonitorInterval to set
	 */
	public void setFdMonitorInterval(Long fdMonitorInterval)
	{
		this.fdMonitorInterval = fdMonitorInterval;
	}

	/**
	 * @return the fdTimeout
	 */
	public Long getFdTimeout()
	{
		return this.fdTimeout;
	}

	/**
	 * @param fdTimeout the fdTimeout to set
	 */
	public void setFdTimeout(Long fdTimeout)
	{
		this.fdTimeout = fdTimeout;
	}

	/**
	 * @return the monitorInterval
	 */
	public synchronized Long getMonitorInterval()
	{
		return this.monitorInterval;
	}

	/**
	 * @param monitorInterval the monitorInterval to set
	 */
	public synchronized void setMonitorInterval(Long monitorInterval)
	{
		this.monitorInterval = monitorInterval;
	}

	/**
	 * @param isMonitoring the isMonitoring to set
	 */
	public void setIsMonitoring(AtomicBoolean isMonitoring)
	{
		this.isMonitoring = isMonitoring;
	}
	
	/**
	 * @return the notifierListeners
	 */
	public Collection getNotifierListeners()
	{
		return this.notifierListeners;
	}

	/**
	 * @param notifierListeners the notifierListeners to set
	 */
	public void setNotifierListeners(Collection<NotifierEventListener> notifierListeners)
	{
		this.notifierListeners = notifierListeners;
	}

	/**
	 * @return the timeout
	 */
	public Long getTimeout()
	{
		return this.timeout;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(Long timeout)
	{
		this.timeout = timeout;
	}

	/**
	 * @return the nrOfAttempts
	 */
	public Integer getNrOfAttempts()
	{
		return this.nrOfAttempts;
	}

	/**
	 * @param nrOfAttempts the nrOfAttempts to set
	 */
	public void setNrOfAttempts(Integer nrOfAttempts)
	{
		this.nrOfAttempts = nrOfAttempts;
	}
	
	/**
	 * @return the failureDetectors
	 */
	public Membership getFailureDetectors()
	{
		return this.failureDetectors;
	}

	/**
	 * @param failureDetectors the failureDetectors to set
	 */
	public void setFailureDetectors(Membership failureDetectors)
	{
		this.failureDetectors = failureDetectors;
	}

	public void addMonitorableComponent(MonitorableComponent component)
	{
		this.component = component;
	}
	
	public void addNotifierEventListener(NotifierEventListener listener)
	{
		this.notifierListeners.add(listener);
	}

	public void fireNotifierEvent(NotifierEvent event)
	{
		for (NotifierEventListener listener : this.notifierListeners)
			listener.notify(event);
	}
	
	public void removeNotifierEventListener(NotifierEventListener listener)
	{
		this.notifierListeners.remove(listener);
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.group.Member#getContent()
	 */
	@Override
	public Object getContent()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.group.Member#getOid()
	 */
	@Override
	public String getOid()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		Member member;
		Boolean returnValue = false;
		// TODO Auto-generated method stub
		if(obj instanceof Member)
		{
			member = (Member)obj;
			returnValue = this.oid.equals(member.getOid());
		}
			
		return returnValue;
	}
}