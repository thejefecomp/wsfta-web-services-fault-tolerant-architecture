/**
 * 
 */
package org.wsfta.model.fd;

import java.rmi.RemoteException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.wsfta.model.event.NotifierEvent;
import org.wsfta.model.group.Member;
import org.wsfta.model.group.Membership;
import org.wsfta.model.group.Message;

/**
 * @author Jeferson Luiz Rodrigues Souza
 *
 */
public class PullFailureDetector extends AbstractFailureDetector
{
	private static final long	serialVersionUID	= 3422716316643022506L;
	
	private CheckServiceTimeout threadCheck;
	
	private NextFDMonitor nextFdMonitor;
	
	private class CheckServiceTimeout extends Thread
	{
		@Override
		public void run()
		{
			NotifierEvent event;
			Message message;
			
			int nrTimes = PullFailureDetector.this.nrTimes.incrementAndGet();
			
			if(nrTimes >= PullFailureDetector.this.nrOfAttempts.intValue())
			{
				System.out.println("FALHOU !!!");
				event = new NotifierEvent(PullFailureDetector.this.oid);
				message = new Message(null,PullFailureDetector.this, PullFailureDetector.this.oid);
				PullFailureDetector.this.fireNotifierEvent(event);
				PullFailureDetector.this.isMonitoring.set(false);
				PullFailureDetector.this.getFailureDetectors().sendMessage(PullFailureDetector.this,message);
			}
		}	
	}
	
	private class CheckFDTimeout extends Thread
	{
		@Override
		public void run()
		{
			int nrFdTimes = PullFailureDetector.this.nrFdTimes.incrementAndGet();
			
			if(nrFdTimes >= PullFailureDetector.this.nrOfFDAttempts.intValue())
			{
				PullFailureDetector.this.nextFdMonitor.stopMonitor();
			}
		}
		
	}
	
	private class NextFDMonitor extends Thread
	{
		private ScheduledFuture<?> intervalMonitor;
		
		private ScheduledFuture<?> fdMonitor;
		
		private CheckFDTimeout checkFdTimeout;
		
		public NextFDMonitor()
		{
			this.checkFdTimeout = new PullFailureDetector.CheckFDTimeout();
			this.startMonitor();
		}
		
		public void stopMonitor()
		{
			this.intervalMonitor.cancel(true);
		}
		
		public void startMonitor()
		{
			this.intervalMonitor = PullFailureDetector.this.scheduler.scheduleWithFixedDelay(this, PullFailureDetector.this.getFdMonitorInterval(), PullFailureDetector.this.getFdMonitorInterval(), TimeUnit.MILLISECONDS);
		}
		
		@Override
		public void run()
		{	
			this.fdMonitor = PullFailureDetector.this.scheduler.schedule(this.checkFdTimeout, PullFailureDetector.this.getFdTimeout(), TimeUnit.MILLISECONDS);
		/*	Member fdNext = PullFailureDetector.this.getNextFD(PullFailureDetector.this.oid);
			Long idReply;
			
			
			try
			{
				PullFailureDetector.this.getFailureDetectors().sendMessage(message, fdNext);
				
				if(PullFailureDetector.this.nrFdTimes.get() == 0)
					this.fdMonitor.cancel(true);
				else
					PullFailureDetector.this.nrFdTimes.decrementAndGet();
			}
			catch (RemoteException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/	
		}
	}
	
	private Member getNextFD(String oid)
	{
		Member next = null;
		Boolean isBreak = false;
		
		for (Member member : PullFailureDetector.this.getFailureDetectors().getChannel().getMembership())
		{
			if(isBreak)
			{
				next = member;
				break;
			}
			
			if(member.getOid().equals(oid))
				isBreak = true;
		}
		
		return next;
	}
	
	public PullFailureDetector()
	{
		this.threadCheck = new CheckServiceTimeout();
		//this.nextFdMonitor = new NextFDMonitor();
	}
	
	@Override
	public void run()
	{
		/*try
		{
			for (int i = 0; i < 5; i++)
				this.component.getStatus();
		}
		catch(RemoteException e)
		{
			e.printStackTrace();
		}*/
		// TODO Auto-generated method stub
		while (this.isMonitoring.get())
		{
			try
			{
				this.component.getStatus();
			}
			catch (RemoteException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally
			{
				this.checkTimeout = this.scheduler.schedule(this.threadCheck, this.timeout, TimeUnit.MILLISECONDS);
			}
			
			try
			{
				Thread.sleep(this.getMonitorInterval());
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public Object getContent()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getOid()
	{
		// TODO Auto-generated method stub
		return this.oid;
	}
	
	@Override
	public void updateServiceInterval() throws RemoteException
	{
		// TODO Auto-generated method stub
		this.lastReceiveTime = System.currentTimeMillis();
		if(this.nrTimes.get() == 0)
		{
			if(this.checkTimeout != null)
				this.checkTimeout.cancel(false);
		}
		else
			this.nrTimes.decrementAndGet();
	}
	
	@Override
	public void updateFDInterval() throws RemoteException
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void updateMembership(Boolean operationType, String... content)
	{
		// TODO Auto-generated method stub
		if(operationType.equals(Membership.REMOVE))
		{
			Member member = null;
			for (Member m : this.getFailureDetectors().getChannel().getMembership())
			{
				if(m.getOid().equals(content[0]))
				{
					member = m;
					break;
				}		
			}
			try
			{
				this.updateMembership(operationType, member);
			}
			catch (RemoteException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void updateMembership(Boolean operationType, Member member) throws RemoteException
	{
		// TODO Auto-generated method stub
		if(member != null)
			this.getFailureDetectors().getChannel().getMembership().remove(member);
	}
}