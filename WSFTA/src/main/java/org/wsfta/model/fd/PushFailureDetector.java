/**
 * 
 */
package org.wsfta.model.fd;

import java.rmi.RemoteException;
import java.util.concurrent.TimeUnit;

import org.wsfta.model.event.NotifierEvent;
import org.wsfta.model.group.Member;

/**
 * @author Jeferson
 *
 */
public class PushFailureDetector extends AbstractFailureDetector
{		
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		NotifierEvent event;
		
		int times = this.nrTimes.incrementAndGet();
		if(times == this.nrOfAttempts)
		{
			event = new NotifierEvent(this);
			this.fireNotifierEvent(event);
			this.isMonitoring.set(false);
		}
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.FailureDetector#updateInterval()
	 */
	
	public void updateInterval() throws RemoteException
	{
		// TODO Auto-generated method stub
		this.lastReceiveTime = System.currentTimeMillis();
		
		if(this.isMonitoring.get())
		{
			if(this.nrTimes.get() == 0 && this.checkTimeout != null)
				this.checkTimeout.cancel(false);
			else
				this.nrTimes.decrementAndGet();
			
			this.checkTimeout = this.scheduler.schedule(this, this.getTimeout(), TimeUnit.MILLISECONDS);
		}		
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.group.Member#getContent()
	 */
	@Override
	public Object getContent()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.group.Member#getOid()
	 */
	@Override
	public String getOid()
	{
		// TODO Auto-generated method stub
		return this.oid;
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.FailureDetector#updateFDInterval()
	 */
	@Override
	public void updateFDInterval() throws RemoteException
	{
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.FailureDetector#updateMembership(java.lang.Boolean, java.lang.String[])
	 */
	@Override
	public void updateMembership(Boolean operationType, String... content)
	{
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.FailureDetector#updateMembership(java.lang.Boolean, org.wsfta.model.group.Member)
	 */
	@Override
	public void updateMembership(Boolean operationType, Member member)
			throws RemoteException
	{
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.fd.FailureDetector#updateServiceInterval()
	 */
	@Override
	public void updateServiceInterval() throws RemoteException
	{
		// TODO Auto-generated method stub
		
	}
}