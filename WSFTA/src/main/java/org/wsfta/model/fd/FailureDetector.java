/**
 * 
 */
package org.wsfta.model.fd;

import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.jws.WebMethod;

import org.wsfta.model.group.Member;

/**
 * @author Jeferson
 *
 */
public interface FailureDetector extends Remote, Runnable
{
	public static String RFD = "RFD";
	
	public static String GFD = "GFD";
	
	@WebMethod
	public void updateServiceInterval() throws RemoteException;
	
	@WebMethod
	public void updateFDInterval() throws RemoteException;
	
	@WebMethod
	public void updateMembership(Boolean operationType, String... content);
	
	public void updateMembership(Boolean operationType, Member member) throws RemoteException;
}