package org.wsfta.model.fd;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.wsfta.model.group.Member;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 12/2006
 */
public interface MonitorableComponent extends Remote, Runnable
{
	public void getStatus() throws RemoteException;
	
	public void updateFD();
	
	public void updateMembership(Member member, String fdType, Boolean operationType);
	
	public void setDetector(FailureDetector detector);
	
	public void setHeartbeatInterval(Long heartbeatInterval);
}
