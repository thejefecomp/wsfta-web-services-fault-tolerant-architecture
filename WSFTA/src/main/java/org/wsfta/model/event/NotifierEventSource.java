/**
 * 
 */
package org.wsfta.model.event;

import java.util.List;

/**
 * @author jeferson
 *
 */
public abstract class NotifierEventSource
{
    private List<NotifierEventListener> listeners;

    public List<NotifierEventListener> getListeners()
    {
        return this.listeners;
    }

    public void setListeners(List<NotifierEventListener> listeners)
    {
        this.listeners = listeners;
    }
    
    public void addListener(NotifierEventListener listener)
    {
        this.listeners.add(listener);
    }
    
    public boolean removeListener(NotifierEventListener listener)
    {
        return this.listeners.remove(listener);
    }
    
    public void notifyListeners(NotifierEvent event)
    {
        for (NotifierEventListener listener : this.listeners)
            listener.notify(event);
    }
}