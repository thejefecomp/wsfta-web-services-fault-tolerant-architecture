package org.wsfta.model.event;

public interface NotifierEventListener
{
	public abstract void notify(NotifierEvent event);
}
