package org.wsfta.model.event;

import java.util.EventObject;

public class NotifierEvent extends EventObject
{
    /**
     * 
     */
    private static final long serialVersionUID = 9096063219105966485L;

    private Object data;
    
    /**
     * @param source
     */
    public NotifierEvent(Object source)
    {
    	super(source);
    	// TODO Auto-generated constructor stub
    }
    
    public Object getData()
    {
    	return this.data;
    }

    public void setData(Object data)
    {
    	this.data = data;
    }
}
