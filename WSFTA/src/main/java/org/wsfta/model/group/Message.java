package org.wsfta.model.group;

import java.io.Serializable;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 11/2006
 */
public class Message implements Serializable, Comparable<Message>
{
	private static final long serialVersionUID = 1712533364756243740L;

	private Object content;

	private String source;

	private Long oid;

	public Message(Long oid, String source, Object content)
	{
		this.oid = oid;
		this.source = source;
		this.content = content;
	}

	public Object getContent()
	{
		return this.content;
	}

	public void setContent(Object content)
	{
		this.content = content;
	}

	public String getSource()
	{
		return this.source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public Long getOid()
	{
		return this.oid;
	}

	public void setOid(Long oid)
	{
		this.oid = oid;
	}

    @Override
    public boolean equals(Object obj)
    {
        // TODO Auto-generated method stub
        Message msg;
        
        if(obj instanceof Message)
            msg = (Message) obj;
        else
            return false;
        
        return this.oid.equals(msg.oid) && this.source.equals(msg.source);
    }

    @Override
    public int compareTo(Message msg)
    {
        // TODO Auto-generated method stub
        return this.oid.compareTo(msg.oid) + msg.source.compareTo(this.source);
    }   
}