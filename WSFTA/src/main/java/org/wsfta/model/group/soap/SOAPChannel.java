package org.wsfta.model.group.soap;

import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.Service.Mode;
import javax.xml.ws.soap.SOAPBinding;

import org.wsfta.model.event.NotifierEvent;
import org.wsfta.model.event.NotifierEventSource;
import org.wsfta.model.group.Channel;
import org.wsfta.model.group.Member;
import org.wsfta.model.group.Message;
import org.wsfta.model.group.exception.NoSuchMemberException;
import org.wsfta.model.ordering.MessageMarker;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 01/2007
 */
@ServiceMode(Mode.MESSAGE)
@WebServiceProvider()
public class SOAPChannel extends NotifierEventSource implements Channel, Provider<SOAPMessage>
{
    private Integer index;
    
    private SOAPConnection soapConnection;

    private MessageFactory messageFactory;

    private Member member;

    private List<Member> membership;
    
    private MessageMarker messageMarker;

    private ExecutorService pool;

    private Deque<Message> recvBuffer;

    private final Lock recvLock = new ReentrantLock();

    private final Condition isEmpty = this.recvLock.newCondition();

    private final Pattern pattern = Pattern.compile("\\d+,\\w+");

    private class SOAPHandler implements Runnable
    {
        private SOAPMember member;

        private Message message;

        private String makeOrderList(List<Message> messages)
        {
            StringBuilder buffer = new StringBuilder("{");

            for (Message message : messages)
            {
                buffer.append("{");
                buffer.append(message.getOid());
                buffer.append(",");
                buffer.append(message.getSource());
                buffer.append("}");
                buffer.append(",");
            }

            buffer.insert(buffer.length() - 1, "}");
            return buffer.toString();

        }

        @SuppressWarnings("unchecked")
        private SOAPMessage handleMessage(Message message) throws SOAPException
        {
            SOAPMessage soapMessage = null;

            if (this.message.getContent() instanceof SOAPMessage) 
                soapMessage = (SOAPMessage) message.getContent();
            
            else if (this.message.getContent() instanceof List)
            {
                soapMessage = SOAPChannel.this.messageFactory.createMessage();
                SOAPHeader header = soapMessage.getSOAPPart().getEnvelope().getHeader();
                QName orderList = header.createQName("wsfta-orderList", header.getElementName().getPrefix());
                header.addHeaderElement(orderList).setValue(this.makeOrderList((List<Message>) this.message.getContent()));
                soapMessage.saveChanges();
            }

            return soapMessage;
        }

        public SOAPHandler(Message message, Member member)
        {
            this.member = (SOAPMember) member;
            this.message = message;
        }

        /**
         * @see java.lang.Runnable#run()
         */
        public void run()
        {
            // TODO Auto-generated method stub
            try
            {
                SOAPMessage soapMessage = this.handleMessage(this.message);

                if (this.message.getContent() instanceof List)
                {
                    Service service = Service.create(this.member.getServiceName());
                    service.addPort(this.member.getPortName(), SOAPBinding.SOAP11HTTP_BINDING, this.member.getEndpointAddress().toString());
                    Dispatch<SOAPMessage> dispatcher = service.createDispatch(this.member.getPortName(), SOAPMessage.class, Service.Mode.MESSAGE);
                    dispatcher.invokeOneWay(soapMessage);
                }
                else
                {
                    SOAPMessage replySoapMessage = null;
                    Object[] content = new Object[2];
                    content[0] = this.member.getOid();
                    Message replyMessage = new Message(this.message.getOid(),this.message.getSource(), null);
                    replySoapMessage = SOAPChannel.this.soapConnection.call(soapMessage, this.member.getEndpointAddress());
                    content[1] = replySoapMessage;
                    replyMessage.setContent(content);
                    SOAPChannel.this.recvLock.lock();
                    SOAPChannel.this.recvBuffer.addLast(replyMessage);
                    SOAPChannel.this.isEmpty.signalAll();
                    SOAPChannel.this.recvLock.unlock();
                }
            }
            catch (SOAPException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public SOAPChannel() throws SOAPException
    {
        this.soapConnection = SOAPConnectionFactory.newInstance().createConnection();
        this.messageFactory = MessageFactory.newInstance();
    }

    @Override
    public Member getMember()
    {
        // TODO Auto-generated method stub
        return this.member;
    }

    @Override
    public void setMember(Member member)
    {
        // TODO Auto-generated method stub
        this.member = member;
        this.index = this.membership.indexOf(this.member);
    }

    /**
     * @see org.wsfta.model.group.Channel#getMembership()
     */
    public List<Member> getMembership()
    {
        return this.membership;
    }

    /**
     * @see org.wsfta.model.group.Channel#setMembership(java.util.Vector)
     */
    public void setMembership(List<Member> membership)
    {
        Collections.sort(membership);
        this.membership = membership;
    }

    @Override
    public void setMessageMarker(MessageMarker messageMarker)
    {
        // TODO Auto-generated method stub
        this.messageMarker = messageMarker;
    }
    
    public void setPool(ExecutorService pool)
    {
        this.pool = pool;
    }

    /**
     * @param recvBuffer
     *            the recvBuffer to set
     */
    public void setRecvBuffer(Deque<Message> recvBuffer)
    {
        this.recvBuffer = recvBuffer;
    }

    /**
     * @see org.wsfta.model.group.Channel#sendMessage(org.wsfta.model.group.Message)
     */
    @Override
    public void sendMessage(Message message)
    {

        for (Member m : this.membership)
        {
            if (!this.member.getOid().equals(m.getOid()))
            {
                try
                {
                    this.sendMessage(message, m);
                }
                catch (NoSuchMemberException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @see org.wsfta.model.group.Channel#sendMessage(org.wsfta.model.group.Message,
     *      org.wsfta.model.group.soap.SOAPMember)
     */
    public void sendMessage(Message message, Member member) throws NoSuchMemberException
    {
        if (!this.membership.contains(member)) throw new NoSuchMemberException();

        this.pool.execute(new SOAPHandler(message, member));
    }

    /**
     * @see org.wsfta.model.group.Channel#receiveMessage(org.wsfta.model.group.soap.SOAPMember)
     */
    @SuppressWarnings("finally")
    public Message receiveMessage(Member member) throws NoSuchMemberException
    {
        Message msg = null;
        boolean nonAvailable = true;
        this.recvLock.lock();
        try
        {
            while (nonAvailable)
            {
                while (this.recvBuffer.size() == 0)
                    this.isEmpty.await();

                for (Message message : this.recvBuffer)
                {
                    if (member.equals(message.getSource()))
                    {
                        msg = message;
                        nonAvailable = false;
                        this.recvBuffer.remove(message);
                        break;
                    }
                }

                if (nonAvailable) this.isEmpty.await();
            }
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally
        {
            this.recvLock.unlock();
            return msg;
        }
    }

    /**
     * @see org.wsfta.model.group.Channel#receiveMessage()
     */
    @SuppressWarnings("finally")
    @Override
    public Message receiveMessage()
    {
        Message msg = null;
        this.recvLock.lock();

        try
        {
            while (this.recvBuffer.size() == 0)
                this.isEmpty.await();

            msg = this.recvBuffer.poll();
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally
        {
            this.recvLock.unlock();
            return msg;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void notify(NotifierEvent event)
    {   
        if(event.getData() instanceof Message)
            this.sendMessage((Message) event.getData());       
        else if (event.getData() instanceof List && !(event.getSource() instanceof Member))
        {
            Message message = null;
            List<Message> messageOrderList = (List<Message>) event.getData();
            
            /*Source = null significa que a lista de ordenação foi gerada pelo membro que está associado a este canal. A outra condicional obedece a propriedade de grupos de réplicas
             * da WSFTA. Todo grupo de réplicas, com exceção do grupo virtual de líderes, deve possuir somente um líder. 
             */
            if(event.getSource() == null)
            {
                message = new Message(null, null,messageOrderList);
                this.sendMessage(message);
            }
            
            
            this.recvLock.lock();
            for (Message msg : messageOrderList)
                this.recvBuffer.addLast(msg);
            
            this.isEmpty.signalAll();
            this.recvLock.unlock();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public SOAPMessage invoke(SOAPMessage request)
    {
        // TODO Auto-generated method stub
        Message message = null;
        QName wsftaIdName = null;
        QName wsftaMemberName = null;
        SOAPEnvelope soapEnvelope = null;
        SOAPHeader soapHeader = null;
        message = new Message(null, null, null);
        Iterator<SOAPElement> iterator = null;
        NotifierEvent event = new NotifierEvent(this.member);
        boolean instantlyReply = false;
        
        try
        {
            soapEnvelope = request.getSOAPPart().getEnvelope();
            soapHeader = soapEnvelope.getHeader() == null ? soapEnvelope.addHeader() : soapEnvelope.getHeader();
            wsftaIdName = soapHeader.createQName("wsfta-id", soapHeader.getElementName().getPrefix());
            wsftaMemberName = soapHeader.createQName("wsfta-member",soapHeader.getElementName().getPrefix());
            QName wsftaOrderList = soapHeader.createQName("wsfta-orderList", soapHeader.getElementName().getPrefix());
    
            if (soapHeader.getChildElements(wsftaIdName).hasNext())
            {
                iterator = soapHeader.getChildElements(wsftaIdName);
                message.setOid(Long.valueOf(iterator.next().getValue()));
                iterator = soapHeader.getChildElements(wsftaMemberName);
                message.setSource(iterator.next().getValue());
                message.setContent(request);
                event.setData(message);
            }
            else if (soapHeader.getChildElements(wsftaOrderList).hasNext())
            {
                iterator = soapHeader.getChildElements(wsftaOrderList);
                Matcher m = pattern.matcher(iterator.next().getValue());
                String[] data = null;
                List<Message> orderList = new LinkedList<Message>();
    
                while (m.find())
                {
                    data = m.group().split(",");
                    message = new Message(Long.valueOf(data[0]), data[1],null);
                    orderList.add(message);
                }
    
                event.setData(orderList);
                instantlyReply = true;
            }
            else
            {
                message.setOid(this.messageMarker.getNextId());
                soapHeader.addHeaderElement(wsftaIdName).setValue(message.getOid().toString());
                soapHeader.addHeaderElement(wsftaMemberName).setValue(message.getSource());
                request.saveChanges();
                this.sendMessage(message);
                message.setContent(request);
                message.setSource(this.member.getOid());
                event.setData(message);
            }
            
            this.notifyListeners(event);
        }
        catch (SOAPException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        if(instantlyReply)
            return null;
        else
        {
            //Waiting for the request execution and others actions executed to the WSFTA components;
            
            return null; // In this case, the return statement must be returns the service response.
        }   
    }
}