package org.wsfta.model.group;

import java.util.Collection;

import org.wsfta.model.group.exception.NoSuchMemberException;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 11/2006
 */
public class MembershipImpl implements Membership
{
    Channel channel; 
    
    @Override
    public Channel getChannel()
    {
        // TODO Auto-generated method stub
        return this.channel;
    }

    @Override
    public void setChannel(Channel channel)
    {
        // TODO Auto-generated method stub
        this.channel = channel;
    }

    /**
	 * @see org.wsfta.model.group.Membership#join(java.lang.String,
	 *      org.wsfta.model.group.soap.SOAPMember)
	 */
	@Override
	public boolean join(Member member)
	{
		// TODO Auto-generated method stub
		Collection<Member> membership = this.channel.getMembership();

		return membership.contains(member) ? false : membership.add(member);
	}

	/**
	 * @see org.wsfta.model.group.Membership#leave(java.lang.String,
	 *      org.wsfta.model.group.soap.SOAPMember)
	 */
	@Override
	public boolean leave(Member member)
	{
		// TODO Auto-generated method stub
		Collection<Member> membership = this.channel.getMembership();
		return membership.remove(member);
	}

	/**
	 * @see org.wsfta.model.group.Membership#receiveMessage()
	 */
	@Override
	public Message receiveMessage()
	{
		// TODO Auto-generated method stub
		return this.channel.receiveMessage();
	}

	/**
	 * @see org.wsfta.model.group.Membership#receiveMessage(org.wsfta.model.group.soap.SOAPMember)
	 */
	@Override
	public Message receiveMessage(Member member)throws NoSuchMemberException
	{
		// TODO Auto-generated method stub
		return this.channel.receiveMessage(member);
	}

	/**
	 * @see org.wsfta.model.group.Membership#sendMessage(org.wsfta.model.group.Message)
	 */
	@Override
	public void sendMessage(Message message)
	{
		// TODO Auto-generated method stub
		this.channel.sendMessage(message);
	}

	/**
	 * @see org.wsfta.model.group.Membership#sendMessage(org.wsfta.model.group.Message,
	 *      org.wsfta.model.group.soap.SOAPMember)
	 */
	@Override
	public void sendMessage(Message message, Member member)throws NoSuchMemberException
	{
		// TODO Auto-generated method stub
	    this.channel.sendMessage(message, member);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.wsfta.model.group.Membership#size()
	 */
	@Override
	public Integer size()
	{
		// TODO Auto-generated method stub
		return this.channel.getMembership().size();
	}
}