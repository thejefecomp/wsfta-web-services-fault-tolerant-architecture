package org.wsfta.model.group;

import java.util.List;

import org.wsfta.model.event.NotifierEventListener;
import org.wsfta.model.group.exception.NoSuchMemberException;
import org.wsfta.model.ordering.MessageMarker;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 01/2007
 */
public interface Channel extends NotifierEventListener
{
    public void setMessageMarker(MessageMarker messageMarker);
    
    /**
     * 
     * @return
     */
    public Member getMember();
    
    /**
     * 
     * @param member
     */
    public void setMember(Member member);
    
	/**
	 * @return
	 */
	public List<Member> getMembership();

	/**
     * @param membership
     */
    public void setMembership(List<Member> membership);
	
	/**
	 * @param message
	 */
	public void sendMessage(Message message);

	/**
	 * @param message
	 * @param member
	 */
	public void sendMessage(Message message, Member member)throws NoSuchMemberException;

	/**
	 * @param member
	 * @return
	 */
	public Message receiveMessage(Member member)throws NoSuchMemberException;

	/**
	 * @return
	 */
	public Message receiveMessage();
}