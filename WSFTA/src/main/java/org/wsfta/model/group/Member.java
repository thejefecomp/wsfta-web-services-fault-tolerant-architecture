/**
 * 
 */
package org.wsfta.model.group;

import java.io.Serializable;

/**
 * @author Jeferson Luiz Rodrigues Souza
 *
 */
public interface Member extends Serializable, Comparable<Member>
{
    public static final Boolean LEADER = true;
    
    public static final Boolean MEMBER = false;
    
    public Boolean getisLeader();
    
	public String getOid();
	
	public Object getContent();
}