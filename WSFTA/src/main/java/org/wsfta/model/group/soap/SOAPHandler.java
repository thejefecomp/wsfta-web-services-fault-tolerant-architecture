package org.wsfta.model.group.soap;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;

import org.wsfta.model.group.Member;
import org.wsfta.model.group.Message;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 01/2007
 */
public class SOAPHandler implements Runnable
{
    private SOAPMember member;

	private Message message;
	
	@SuppressWarnings("unchecked")
	private SOAPMessage handleMessage(Message message) throws SOAPException
	{
		SOAPMessage soapMessage = null;
		
		if(message.getContent() instanceof SOAPMessage)
			soapMessage = (SOAPMessage)message.getContent();
		else if(message.getContent() instanceof List)
		{
		    
		}
		
		return soapMessage;
	}
	
	public SOAPHandler(Message message, Member member)
	{
		this.member = (SOAPMember)member;
		this.message = message;
	}

	/**
     * @see java.lang.Runnable#run()
     */
	public void run()
	{
		// TODO Auto-generated method stub
		SOAPMessage replySoapMessage = null;
		Message replyMessage = new Message(null,null,null);
		try
		{
			SOAPMessage soapMessage = this.handleMessage(this.message);
//			replySoapMessage = SOAPChannel.this.soapConnection.call(soapMessage, this.member.getEndpointAddress());
//			replyMessage.setSource(this.member);
			replyMessage.setContent(replySoapMessage);
		}
		catch (SOAPException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
} 