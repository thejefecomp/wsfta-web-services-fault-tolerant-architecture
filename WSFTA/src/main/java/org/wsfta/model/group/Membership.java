package org.wsfta.model.group;


import org.wsfta.model.group.exception.NoSuchMemberException;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 11/2006
 */
public interface Membership
{	

	/**
	 * Retorna um objeto do tipo {@link org.wsfta.model.group.Channel} que representa o canal de comunicação utilizado para troca de mensagens do grupo.
	 * @return Um objeto do tipo {@link org.wsfta.model.group.Channel}.
	 */
	public Channel getChannel();
	
	/**
     * Método que atribui um canal de comunicação para troca de mensagens do grupo.
     * @param channel Objeto do tipo {@link org.wsfta.model.group.Channel}
     */
    public void setChannel(Channel channel);
	
	/**
	 * Adiciona um membro ao grupo.
	 * @param member Membro que será adicionado ao grupo
	 * @return <code>true</code> se o membro pode ser adicionado com sucesso e <code>false</code> caso contrário.
	 */
	public boolean join(Member member);

	/**
	 * Remove um membro do grupo.
	 * @param member membro que será removido do grupo.
	 * @return <code>true</code> se o membro pode ser removido com sucesso e <code>false</code> caso contrário.
	 */
	public boolean leave(Member member);

	/**
	 * Método responsável por enviar uma mensagem para todos os membros do grupo.
	 * @param message Mesagem que será enviada para todos os membros do grupo.
	 */
	public void sendMessage(Message message);

	/**
	 * Método responsável por enviar uma mensagem para um membro específico do grupo.
	 * @param message
	 * @param member
	 */
	public void sendMessage(Message message, Member member) throws NoSuchMemberException;

	/**
	 * 
	 * @return
	 */
	public Message receiveMessage();

	/**
	 * 
	 * @param member
	 * @return
	 */
	public Message receiveMessage(Member member) throws NoSuchMemberException;
	
	/**
	 * 
	 * @return
	 */
	public Integer size();
}