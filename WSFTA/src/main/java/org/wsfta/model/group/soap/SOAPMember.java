package org.wsfta.model.group.soap;

import java.net.URL;

import javax.xml.namespace.QName;

import org.wsfta.model.group.Member;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 11/2006
 */
public class SOAPMember implements Member
{
	private static final long	serialVersionUID	= 1430477389243304112L;

	private String oid;

	private URL endpointAddress;

	private QName portName;
	
	private QName serviceName;
	
	private Boolean leader;

	/**
	 * Construtor da classe Member responsável pela inicialização de um Membro da WSFTA.
	 * @param oid oid que identifica unicamente o objeto que representa esse membro.
	 * @param address Endereço de rede onde o membro está localizado.
	 * @param port Número da porta onde o serviço recebe as requisições enviadas para o endereço de rede representado pelo atributo <code>address</code>.
	 */
	public SOAPMember(String oid, URL endpointAddress, QName portName, QName serviceName, Boolean leader)
	{
		this.oid = oid;
		this.endpointAddress = endpointAddress;
		this.portName = portName;
		this.serviceName = serviceName;
		this.leader = leader;
	}

	/**
	 * Método que retorna o endereço que indica a localização do serviço que esse membro representa.
	 * @return O endereço onde esse membro está localizado.
	 */
	public URL getEndpointAddress()
	{
		return this.endpointAddress;
	}

	@Override
    public Boolean getisLeader()
    {
        // TODO Auto-generated method stub
        return this.leader;
    }
	
	/**
	 * Método que retorna o oid que identifica unicamente esse objeto.
	 * @return O oid que identifica unicamente esse objeto.
	 */
	@Override
	public String getOid()
	{
		return this.oid;
	}

	/**
	 * M�todo que retorna o nome da porta indicada no arquivo WSDL do servi�o que esse membro representa.
	 * @return Nome da porta indicada no arquivo WSDL do servi�o que esse membro representa.
	 */
	public QName getPortName()
	{
		return this.portName;
	}

	/**
	 * M�todo que retorna  nome do servi�o que esse membro representa. Esse nome � indicado no arquivo WSDL para acesso ao mesmo.
	 * @return Nome do servi�o que esse membro representa.
	 */
	public QName getServiceName()
	{
		return this.serviceName;
	}

	/* (non-Javadoc)
	 * @see org.wsfta.model.group.Member#getContent()
	 */
	@Override
	public Object getContent()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		Member member;
		Boolean returnValue = false;
		
		if(obj instanceof Member)
		{
			member = (Member)obj;
			returnValue = this.oid.equals(member.getOid());
		}
		
		return returnValue;
	}

    @Override
    public int compareTo(Member member)
    {
        // TODO Auto-generated method stub
        return this.oid.compareTo(member.getOid());
    }
}