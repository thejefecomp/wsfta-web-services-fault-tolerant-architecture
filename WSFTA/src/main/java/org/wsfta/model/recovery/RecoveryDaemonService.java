package org.wsfta.model.recovery;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.wsfta.model.group.Membership;

@WebService
public abstract class RecoveryDaemonService implements Runnable 
{ 
	private Membership recoveryMembership;
	
	@WebMethod
	public abstract void startMemberRecovery();
	
	public abstract void startGroupRecovery();
		
	@WebMethod
	public abstract void startGroupRecovery(Integer nrElements);
		
	public void setRecoveryMembership(Membership membership) 
	{
		this.recoveryMembership = membership;
	
	}
		 
	public Membership getRecoveryMembership() 
	{
		return this.recoveryMembership;
	} 
}