/**
 * 
 */
package org.wsfta.model.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.wsfta.model.event.NotifierEventListener;
import org.wsfta.model.fd.AbstractFailureDetector;
import org.wsfta.model.fd.FailureDetector;
import org.wsfta.model.fd.MonitorableComponent;
import org.wsfta.model.fd.PullFailureDetector;
import org.wsfta.model.fd.PushFailureDetector;
import org.wsfta.model.fd.soap.wrapper.WebServiceWrapper;
import org.xml.sax.SAXException;

/**
 * @author Jeferson
 *
 */
public class FDService
{
	private FailureDetector rfd;
	
	private FailureDetector gfd;
	
	private FailureDetector createFD(XPath xpath, Node fdNode) throws XPathExpressionException, MalformedURLException, UnsupportedOperationException, SOAPException
	{
		AbstractFailureDetector fd = null;
		
		if(xpath.evaluate("monitor-style", fdNode).equals("push"))
			fd = new PushFailureDetector();
		else
			fd = new PullFailureDetector();
		
		fd.setMonitorInterval(new Long(xpath.evaluate("monitor-interval", fdNode)));
		fd.setNrOfAttempts(new Integer(xpath.evaluate("nrOfAttempts", fdNode)));
		fd.setOid(xpath.evaluate("/wsfta-member/oid", fdNode.getOwnerDocument()));
		fd.setTimeout(new Long(xpath.evaluate("timeout", fdNode)));
		fd.setScheduler(Executors.newSingleThreadScheduledExecutor());
		fd.setIsMonitoring(new AtomicBoolean(true));
		fd.setNrTimes(new AtomicInteger(0));
		MonitorableComponent component = new WebServiceWrapper(new URL(xpath.evaluate("/wsfta-member/url", fdNode.getOwnerDocument())), Executors.newCachedThreadPool(), SOAPConnectionFactory.newInstance().createConnection(), MessageFactory.newInstance());
		System.out.println("Endereco: --> "+xpath.evaluate("/wsfta-member/url", fdNode.getOwnerDocument()));
		component.setDetector(fd);
		fd.addMonitorableComponent(component);
		fd.setNotifierListeners(new ArrayList<NotifierEventListener>());
		return fd;
	}
	
	private void startService()
	{
		if(this.rfd instanceof PullFailureDetector)
			new Thread(this.rfd).start();
	}
	
	/**
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws XPathExpressionException 
	 * @throws SOAPException 
	 * @throws UnsupportedOperationException 
	 * 
	 */
	public FDService(String memberFilename) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, UnsupportedOperationException, SOAPException
	{
		// TODO Auto-generated constructor stub
		XPathFactory xPathFactory = XPathFactory.newInstance();
		XPath xPath = xPathFactory.newXPath();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(this.getClass().getClassLoader().getResourceAsStream(memberFilename));
		NodeList failureDetectors = (NodeList) xPath.evaluate("//failure-detector", doc, XPathConstants.NODESET);
		//Boolean isLeader = (Boolean) xPath.evaluate("/wsfta-member/isLeader", doc, XPathConstants.BOOLEAN);
		Node fd = null;
		for (int i = failureDetectors.getLength(); i > 0; i--)
		{
			fd = failureDetectors.item(i-1);
			if(xPath.evaluate("type", fd).equals("RFD"))
				this.rfd = this.createFD(xPath, fd);
			else
				this.gfd = this.createFD(xPath, fd);
		}
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		try
		{
			FDService fdService = new FDService("member.xml");
			
			fdService.startService();
			
			System.out.println("pressione qualquer tecla para terminar a aplicacao !!!");
			System.in.read();
			
			
		}
		catch (XPathExpressionException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (UnsupportedOperationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ParserConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SOAPException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
