/**
 * 
 */
package org.wsfta.model.util;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

/**
 * @author Jeferson Luiz Rodrigues Souza
 * @version 1.0
 * @since 02/2007
 */
public class ServiceWrapper
{
	public Service create(QName serviceName)
	{
		return Service.create(serviceName);
	}
	
	public Service create(URL wsdlLocation, QName serviceName)
	{
		return Service.create(wsdlLocation, serviceName);
	}
}
