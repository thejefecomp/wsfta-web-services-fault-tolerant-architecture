/**
 * 
 */
package org.wsfta.model.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.wsfta.model.event.NotifierEventListener;
import org.wsfta.model.group.Channel;
import org.wsfta.model.group.Member;
import org.wsfta.model.group.Membership;
import org.wsfta.model.group.MembershipImpl;
import org.wsfta.model.group.Message;
import org.wsfta.model.group.soap.SOAPChannel;
import org.wsfta.model.group.soap.SOAPMember;
import org.wsfta.model.ordering.MessageMarker;
import org.wsfta.model.ordering.MessageOrder;
import org.wsfta.model.ordering.impl.MessageMarkerImpl;
import org.wsfta.model.service.ServiceManager;
import org.wsfta.model.service.ServiceManagerImpl;
import org.xml.sax.SAXException;


/**
 * @author Jeferson
 *
 */
public class Initialize
{	
	private Membership initializeMembership(NodeList members, Channel channel, Boolean leaders) throws XPathExpressionException, DOMException, ParserConfigurationException, SAXException, IOException
	{
		Membership membership = new MembershipImpl();
		XPathFactory xPathFactory = XPathFactory.newInstance();
		XPath xPath = xPathFactory.newXPath();
		String oid = null;
		String namespace = null;
		URL endpoint = null;
		QName portName = null; 
		QName serviceName = null;
		SOAPMember member = null;
		boolean leader = true;
		membership.setChannel(channel);
		
		System.out.println("Número Membros: ---> "+members.getLength());
		
		for (int i = 0; i < members.getLength(); i++)
		{
			oid = xPath.evaluate("oid", members.item(i));
			endpoint = new URL(xPath.evaluate("url", members.item(i)));
			namespace = xPath.evaluate("namespace", members.item(i));
			portName = new QName(namespace, xPath.evaluate("port-name", members.item(i)));
			serviceName = new QName(namespace, xPath.evaluate("service-name", members.item(i)));
			if(!leaders)
			    leader = Boolean.parseBoolean(xPath.evaluate("isLeader", members.item(i)));
			
			member = new SOAPMember(oid,endpoint,portName, serviceName, leader);
			membership.join(member);
		}
		
		return membership;
	}
	
	private SOAPChannel InitializeSoapChannel() throws UnsupportedOperationException, SOAPException
	{
		MessageMarker messageMarker = new MessageMarkerImpl();
		SOAPChannel soapChannel = new SOAPChannel();
		
		messageMarker.setSeed(System.currentTimeMillis());
		
		soapChannel.setMembership(new ArrayList<Member>());
		soapChannel.setPool(Executors.newCachedThreadPool());
		
		return soapChannel;
	}
	
	private MessageOrder initializeMessageOrder(Node member)
	{
		//MessageOrderFactory messageOrderFactory = new MessageOrderFactory();
		Map<String, ArrayList<Message>> buffers = new HashMap<String, ArrayList<Message>>();
		Map<String, Long> lastInsertedId = new HashMap<String, Long>();
		
		messageOrder.setLastInsertedId(lastInsertedId);
		messageOrder.setBuffers(buffers);
    	messageOrder.setBuffer(new ArrayList<Message>());
    	messageOrder.setActive(new AtomicBoolean(true));
    	
		return messageOrder;
	}
	
	@SuppressWarnings("finally")
	public ServiceManager boot(Map<String, String> configFileNames) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException
	{
		XPathFactory xPathFactory = XPathFactory.newInstance();
		XPath xPath = xPathFactory.newXPath();
		ClassLoader classLoader = Initialize.class.getClassLoader();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(classLoader.getResourceAsStream(configFileNames.get("member")));
		ArrayList<Message> messageWillExecutedCollection = new ArrayList<Message>();
		ArrayList<NotifierEventListener> notifierListeners = new ArrayList<NotifierEventListener>();
		Node memberNode = doc.getDocumentElement();
		NodeList members = null;
		Boolean isLeader = Boolean.valueOf(xPath.evaluate("isLeader", memberNode));
		ServiceManager manager = null;
		SOAPMember member = null;
		Membership leaderMembership = null;
		Membership groupMembership = null;
		SOAPChannel leaderChannel = null;
		SOAPChannel groupChannel = null;
		MessageOrder messageOrder = null;
		Thread messageOrderThread = null;
		MessageMarker messageMarker = null;
		String namespace = null;
		
		try
		{	
			leaderChannel = this.InitializeSoapChannel();
			doc = db.parse(classLoader.getResourceAsStream(configFileNames.get("leaders")));
			members = (NodeList)xPath.evaluate("/wsfta-group/wsfta-leader", doc, XPathConstants.NODESET);
			leaderMembership = this.initializeMembership(members, leaderChannel, true);
			
			groupChannel = this.InitializeSoapChannel();
			doc = db.parse(classLoader.getResourceAsStream(configFileNames.get("group")));
            members = (NodeList)xPath.evaluate("/wsfta-group/wsfta-leader", doc, XPathConstants.NODESET);
			groupMembership = this.initializeMembership(configFileNames.get("group"), groupChannel);
			
			messageOrder = this.initializeMessageOrder("TotalOrder", leaderChannel.getMembership());
			messageOrder = this.initializeMessageOrder("optimisticOrder", leaderChannel.getMembership());
			
			namespace = xPath.evaluate("namespace", memberNode);
			member = new SOAPMember(xPath.evaluate("oid", memberNode),new URL(xPath.evaluate("url", memberNode)), 
								new QName(namespace, xPath.evaluate("port-name", memberNode)), 
								new QName(namespace, xPath.evaluate("service-name", memberNode)), 
								isLeader);
			messageMarker = new MessageMarkerImpl();
			messageMarker.setSeed(new Long(0));
			manager = new ServiceManagerImpl(member, messageWillExecutedCollection);
			manager.setLeaderMembership(leaderMembership);
			manager.setGroupMembership(groupMembership);
			manager.setNotifierListeners(notifierListeners);
			manager.addNotifierEventListener(messageOrder);
		}
		catch (XPathExpressionException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (DOMException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			return manager;
		}
	}
}