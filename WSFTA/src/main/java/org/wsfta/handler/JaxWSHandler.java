package org.wsfta.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.xpath.XPathExpressionException;

import org.wsfta.model.event.NotifierEvent;
import org.wsfta.model.event.NotifierEventSource;
import org.wsfta.model.group.Member;
import org.wsfta.model.group.Message;
import org.wsfta.model.service.ServiceManager;
import org.wsfta.model.util.Initialize;
import org.xml.sax.SAXException;

public class JaxWSHandler extends NotifierEventSource implements SOAPHandler<SOAPMessageContext>
{	
	private static ServiceManager manager;
	
	static
	{
		Initialize initialize = new Initialize();
		Boolean abort = false;
		
		
		try
		{
			Map<String, String> configFiles = new HashMap<String, String>();
			configFiles.put("leaders", "leaders.xml");
			configFiles.put("group", "group.xml");
			configFiles.put("member", "member.xml");
			
			
			manager = initialize.boot(configFiles); 
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			abort = true;
		}
		catch (XPathExpressionException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			abort = true;
		}
		catch (ParserConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			abort = true;
		}
		finally
		{
			if(abort)
				System.exit(1);
		}
	}
	
	/*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders()
     */
	public Set<QName> getHeaders()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext)
     */
	public void close(MessageContext context)
	{
		// TODO Auto-generated method stub

	}

	/*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext)
     */
	public boolean handleFault(SOAPMessageContext context)
	{
		// TODO Auto-generated method stub
		return false;
	}

	/*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.MessageContext)
     */
	public boolean handleMessage(SOAPMessageContext context)
	{
		// TODO Auto-generated method stub
		Message message = null;
		NotifierEvent event = null;
		Boolean returnValue = true;
		Boolean isOutMessage = (Boolean)context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		
		if(isOutMessage)
		{
			
			if(message != null)
				context.setMessage((SOAPMessage)message.getContent());
		}
		else
		{
		    event = new NotifierEvent(null);
		    event.setData(context.getMessage());
			this.notifyListeners(event);
			try
            {
                manager.waitForOrderExecution(context.getMessage());
            }
            catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
			//Falta tratar mensagens one-way, i.e., mensagens que enviam as listas de ordenação, hearth-beat de detecçãoo de falha, entre outras mensagens.
		}
		
		return returnValue;
	}
}